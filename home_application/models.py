# -*- coding: utf-8 -*-

from django.db import models
import datetime
from django.utils import timezone


class Template(models.Model):
    template_name = models.CharField(verbose_name=u"模板名称", max_length=100)
    biz_id = models.IntegerField(verbose_name=u'业务id', null=True)
    biz_name = models.CharField(verbose_name=u'业务名', max_length=200, null=True)
    template_type = models.CharField(verbose_name=u"模板类型", max_length=1000, null=True)
    operation_matters = models.CharField(verbose_name=u"操作事项", max_length=1000, null=True,blank=True)
    responsible = models.CharField(verbose_name=u"责任人", max_length=500, default='')
    bz = models.TextField(max_length=1000, verbose_name='备注')
    isdel = models.IntegerField(verbose_name='是否删除', default=0)

    class Meta:
        verbose_name = u"模板表"
        db_table = 't_template'


class UpcomingTasks(models.Model):
    biz_id = models.IntegerField(verbose_name=u'业务id', null=True)
    biz_name = models.CharField(verbose_name=u'业务名', max_length=200, null=True)
    template_type = models.CharField(verbose_name=u"模板类型", max_length=1000, null=True)
    template_name = models.CharField(verbose_name=u"模板名称", max_length=100)
    identifier = models.CharField(verbose_name=u'操作识别号', max_length=100)
    create_time = models.DateTimeField(verbose_name=u'创建时间', null=True)
    creater = models.CharField(verbose_name=u'创建者', max_length=100, null=True)
    operater = models.CharField(verbose_name=u'操作者', max_length=100, null=True)
    updater = models.CharField(verbose_name=u'更新者', max_length=100, null=True)
    update_time = models.DateTimeField(verbose_name=u'更新时间', null=True)
    confirmor = models.CharField(verbose_name=u'确认人', max_length=100, null=True, blank=True)
    responsible = models.CharField(verbose_name=u"责任人", max_length=500, default='')
    operation_matters = models.CharField(verbose_name=u"操作事项", max_length=1000, null=True, blank=True)
    bz = models.TextField(max_length=1000, verbose_name='备注')

    class Meta:
        verbose_name = u'代办任务表'
        db_table = 't_upcomingtasks'

