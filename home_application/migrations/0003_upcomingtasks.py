# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home_application', '0002_auto_20190418_1248'),
    ]

    operations = [
        migrations.CreateModel(
            name='UpcomingTasks',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('biz_id', models.IntegerField(null=True, verbose_name='\u4e1a\u52a1id')),
                ('biz_name', models.CharField(max_length=200, null=True, verbose_name='\u4e1a\u52a1\u540d')),
                ('template_type', models.CharField(max_length=1000, null=True, verbose_name='\u6a21\u677f\u7c7b\u578b')),
                ('template_name', models.CharField(max_length=100, verbose_name='\u6a21\u677f\u540d\u79f0')),
                ('identifier', models.CharField(max_length=100, verbose_name='\u64cd\u4f5c\u8bc6\u522b\u53f7')),
                ('create_time', models.DateTimeField(null=True, verbose_name='\u521b\u5efa\u65f6\u95f4')),
                ('creater', models.CharField(max_length=100, null=True, verbose_name='\u521b\u5efa\u8005')),
                ('operater', models.CharField(max_length=100, null=True, verbose_name='\u64cd\u4f5c\u8005')),
                ('updater', models.CharField(max_length=100, null=True, verbose_name='\u66f4\u65b0\u8005')),
                ('update_time', models.DateTimeField(null=True, verbose_name='\u66f4\u65b0\u65f6\u95f4')),
                ('confirmor', models.CharField(max_length=100, null=True, verbose_name='\u786e\u8ba4\u4eba', blank=True)),
                ('responsible', models.CharField(default=b'', max_length=500, verbose_name='\u8d23\u4efb\u4eba')),
                ('operation_matters', models.CharField(max_length=1000, null=True, verbose_name='\u64cd\u4f5c\u4e8b\u9879', blank=True)),
                ('bz', models.TextField(max_length=1000, verbose_name=b'\xe5\xa4\x87\xe6\xb3\xa8')),
            ],
            options={
                'db_table': 't_upcomingtasks',
                'verbose_name': '\u4ee3\u529e\u4efb\u52a1\u8868',
            },
        ),
    ]
