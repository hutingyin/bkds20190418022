# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home_application', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='template',
            name='biz_id',
            field=models.IntegerField(null=True, verbose_name='\u4e1a\u52a1id'),
        ),
        migrations.AlterField(
            model_name='template',
            name='operation_matters',
            field=models.CharField(max_length=1000, null=True, verbose_name='\u64cd\u4f5c\u4e8b\u9879', blank=True),
        ),
    ]
