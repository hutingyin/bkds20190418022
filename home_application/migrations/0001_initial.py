# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Template',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('template_name', models.CharField(max_length=100, verbose_name='\u6a21\u677f\u540d\u79f0')),
                ('biz_name', models.CharField(max_length=200, null=True, verbose_name='\u4e1a\u52a1\u540d')),
                ('template_type', models.CharField(max_length=1000, null=True, verbose_name='\u6a21\u677f\u7c7b\u578b')),
                ('operation_matters', models.CharField(max_length=1000, null=True, verbose_name='\u64cd\u4f5c\u4e8b\u9879')),
                ('responsible', models.CharField(default=b'', max_length=500, verbose_name='\u8d23\u4efb\u4eba')),
                ('bz', models.TextField(max_length=1000, verbose_name=b'\xe5\xa4\x87\xe6\xb3\xa8')),
                ('isdel', models.IntegerField(default=0, verbose_name=b'\xe6\x98\xaf\xe5\x90\xa6\xe5\x88\xa0\xe9\x99\xa4')),
            ],
            options={
                'db_table': 't_template',
                'verbose_name': '\u6a21\u677f\u8868',
            },
        ),
    ]
