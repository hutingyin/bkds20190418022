# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

urlpatterns = patterns(
    'home_application.views',
    (r'^$', 'home'),
    (r'^api/', include('home_application.api.urls')),  # 考试测试接口

    # 考试路由配置
    (r'^record/', include('home_application.record.urls')),  # 任务模板首页
    (r'^add_template/$', 'add_template'),  # 跳转到模板添加页面
    (r'template/$', 'template'),   # 添加模板入库


)
