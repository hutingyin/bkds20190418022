# -*- coding: utf-8 -*-


# from utils import ESB
from common.mymako import render_mako_context, render_json
# import json
from django.http import HttpResponse, HttpResponseRedirect
from home_application import models
from django.shortcuts import redirect
from django.core.urlresolvers import reverse

import sys
reload(sys)
sys.setdefaultencoding('utf8')
import datetime
from conf.default import SITE_URL


def home(request):
    """
    """
    response = {}
    try:
        from utils import ESB
        result = ESB.ESBApi(request).search_business()  # 获取业务
        users_dict = ESB.ESBApi(request).get_all_users()
        users = users_dict['data']
        data = result['data']['info']
        context = {'data': data,'users':users}
    except Exception, e:
        response['result'] = False
        response['code'] = 1
        response['msg'] = '%s' % e
        response['data'] = {}
    return render_mako_context(request, '/home_application/home.html',context)


def add_template(request):
    """
    :param request:
    :return:
    """
    response = {}
    try:
        from utils import ESB
        result = ESB.ESBApi(request).search_business()  # 获取业务
        data = result['data']['info']
        context = {'data': data}
    except Exception, e:
        response['result'] = False
        response['code'] = 1
        response['msg'] = '%s' % e
        response['data'] = {}
    return render_mako_context(request, '/home_application/template_manage.html',context)


def template(request):
    """
    模板入库操作
    :param request:
    :return:
    """
    biz_id = request.GET.get('biz_id','')
    biz_name = request.GET.get('biz_name','')
    template_type = request.GET.get('template_type','')
    template_name = request.GET.get('template_name','')
    responsible = request.user.username
    models.Template.objects.create(
        template_name=template_name,
        biz_id = biz_id,
        biz_name=biz_name,
        template_type = template_type,
        responsible = responsible
    )
    return HttpResponse('success')







