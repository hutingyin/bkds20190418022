#!/urs/bin/env/python
# coding=utf-8
# __author__=${胡胡}


from common.mymako import render_mako_context, render_json
from home_application.utils import ESB


def index(request):
    """
    任务模板首页
    """
    response = {}
    try:
        result = ESB.ESBApi(request).search_business()  # 获取业务
        data = result['data']['info']
        context = {'data': data}
    except Exception, e:
        response['result'] = False
        response['code'] = 1
        response['msg'] = '%s' % e
        response['data'] = {}
    return render_mako_context(request, 'home_application/record/index.html',context)
