#!/urs/bin/env/python
# coding=utf-8
# __author__=${胡胡}


from common.mymako import render_json


def test(request):
    """
    考试测试接口
    :param request:
    :return:
    """
    response = {
        "result": True,
        "message": "success",
        "data": {
            "a": 1,
            "b": 2
        }
    }
    return render_json(response)
