#!/urs/bin/env/python
# coding=utf-8
# __author__=${胡胡}


from conf.default import APP_ID, APP_TOKEN
from django.http.request import HttpRequest
from blueking.component.shortcuts import get_client_by_request, get_client_by_user
from blueking.component.client import ComponentClient


class ESBApi(object):
    """
    带request 参数的ESB
    """
    def __init__(self, param):
        if isinstance(param, HttpRequest):
            self.__client = get_client_by_request(param)
            self.username = param.user.username
        else:
            self.__client = get_client_by_user(param)
            self.username = param
        self.__param = {
            "app_code": APP_ID,
            "app_secret": APP_TOKEN,
            'bk_username': self.username
        }

    def get_all_users(self):
        try:
            param = self.__param
            result = self.__client.bk_login.get_all_users(param)
        except Exception, e:
            result = {'message': e}
        return result

    def search_business(self):
        try:
            kwargs = {
                'fields': ['bk_biz_id', 'bk_biz_name']
            }
            result = self.__client.cc.search_business(**kwargs)   # 获取业务id和业务名
        except Exception, e:
            result = {"message": e}
        return result
